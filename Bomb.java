import greenfoot.*; 
import javax.swing.JOptionPane; // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Bala here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Bomb extends Actor
{
    private Vector force;
    public boolean disp = false;
    
    private Integer angulo;
    private Float velocidad;
 
    public Bomb()
    {
        setPla();
        //force = new Vector(Integer.parseInt(p1Angulo));
        //force.scale(Float.parseFloat(p1Velocidad));
        force = new Vector(angulo);
        force.scale(velocidad);
    } 
    public Bomb(Vector force)
    {
          
        this.force = force;
    
    }
    public void act() 
    {
        try{
           force.add(new Vector(90, 0.4));
            GreenfootImage thisImage = getImage();
            MyWorld w = (MyWorld)getWorld();
            setLocation(getX()+(int)force.getX(), getY()+(int)force.getY());
            setRotation(force.getDirection());    
            if (getX()>=999 || getX()<=0 || getY() >= 599 || getY() <= 0 )
            {
                w.removeObject(this);
            }
        }catch(Exception e){}
    }   
    
    public void setPla()
    {        
        angulo = Integer.parseInt(JOptionPane.showInputDialog("Ángulo:"));
        velocidad = Float.parseFloat(JOptionPane.showInputDialog("Velocidad:"));
    }
}

