import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Texto here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Texto extends Actor
{
       private String prefix;
    private Color color;
    private int size;
    
    public Texto(String prefix)
    {
        this.prefix = prefix;
        size = 14;
        //color = RED;
    }

    public void updateImage(String text)
    {
        GreenfootImage image = new GreenfootImage(200, 25);
        image.setColor(Color.RED);
        image.setFont(new Font(true, true, size));
        image.drawString(prefix + ": " + text, 20, 20);
        setImage(image); 
    }
    
    public void setColor(Color color)
    {
        this.color = color;
    }
    
    public void setSize(int size)
    {
        this.size = size;
    }
}
