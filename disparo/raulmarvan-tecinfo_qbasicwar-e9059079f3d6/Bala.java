import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Bala here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Bala extends Actor
{
    private Vector force;
    public boolean disp = false;
   
    
    public Bala()
    {
        force = new Vector();
    }

    public Bala(Vector force)
    {
        this.force = force;
    }
    
    public void act() 
    {
        force.add(new Vector(90, 0.4));
        GreenfootImage thisImage = getImage();
        World world = getWorld();
        
             setLocation(getX()+(int)force.getX(), getY()+(int)force.getY());
             setRotation(force.getDirection());
        
      
        
    } 
    
}
