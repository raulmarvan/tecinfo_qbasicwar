import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class fuerzaBala here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class fuerzaBala extends Actor
{
    private GreenfootImage image; 
    
    public fuerzaBala()
    {
        this (150, 150);
    }
    
    public fuerzaBala(int difX, int difY)
    {
        image = getImage();
        setImage(difX, difY);
    }
    
    public void setImage(int difX, int difY)
    {
        int direction = (int) Math.toDegrees(Math.atan2(difX, difY));
        int length = (int) Math.sqrt(difX*difX+difY*difY) + 30;

        GreenfootImage img = new GreenfootImage(image);
        img.scale(length, 25);
        setRotation(-direction-90);
        setImage(img);
    }
}
