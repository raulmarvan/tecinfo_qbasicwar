import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Balas here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Balas extends Texto
{
      
    private int balas;
    public int inicial = 3;
    
    public Balas()
    {
        super("Arrows");
        balas = inicial;
        updateImage(""+balas);
    }
    
    public void sub()
    {
        balas -= 1;
        updateImage(""+balas);
    }
    
    public int getNum()
    {
        return balas;
    }
    
    public void restart()
    {
        balas = inicial;
        updateImage(""+balas);
    }
}
