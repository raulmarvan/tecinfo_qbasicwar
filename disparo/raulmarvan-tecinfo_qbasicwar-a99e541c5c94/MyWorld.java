import greenfoot.*; // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import javax.swing.JOptionPane;
import java.util.Random;

/**
 * Write a description of class MyWorld here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class MyWorld extends World
{
    ButtonPlay play = new ButtonPlay(); 
    Logo logo = new Logo();
    GreenfootSound music = new GreenfootSound("song.mp3");
    /**
     * Constructor for objects of class MyWorld.
     * 
     */
    public MyWorld()
    {    
        super(1000, 600, 1);         
        
        addObject(logo,500,100);  
        addObject(play,500,300);        
    }
    
    public void Play()
    {
        this.removeObjects(this.getObjects(Logo.class));
        this.removeObjects(this.getObjects(ButtonPlay.class));
        music.playLoop();
        Player p1 = new Player1();
        Player p2 = new Player2();
        addObject(p1,65,500); 
        addObject(p2,950,500); 
        //Start();
        Structure s;
        Random rand;        
        int x = 255,y = 200;
        for(int i = 0; i < 4; i = i + 1)
        {      
            rand = new Random();
            s = new Structure();
            // Obtain a number between [400 - 450].
            y = rand.nextInt(399 + 1) + 450;
            addObject(s,x,y);
            x = x + 175;
        }        
        //Start();
    }
     
     private void Start()
    {        
        String p1Angulo = JOptionPane.showInputDialog("Ángulo Player 1:");
        String p1Velocidad = JOptionPane.showInputDialog("Velocidad Player 1:");
         //ATAQUE P1
        String p2Angulo = JOptionPane.showInputDialog("Ángulo Player 2:");
        String p2Velocidad = JOptionPane.showInputDialog("Velocidad Player 2:");
    }
    
}
