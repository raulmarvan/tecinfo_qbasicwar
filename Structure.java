import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Structure here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Structure extends Actor
{
    private int life = 100;
    /**
     * Act - do whatever the Structure wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
        MyWorld w = (MyWorld)getWorld();
        if(life<=50)
        {
            setImage(new GreenfootImage("st1.png"));
        }else if(life<=0)
        {                
            w.removeStruct(this);
        }else if(isTouching(Bomb.class))
        {
            w.removeBomb();
            this.quitaLife();
        }
    }
    
    public int getLife()
    {
        return life;
    }
    
    public void quitaLife()
    {
        life = life - 10;
    }
}
