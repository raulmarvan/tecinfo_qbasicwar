    import greenfoot.*; 
    import java.util.*;
    
    /**
     * Write a description of class Player1 here.
     * 
     * @author (your name) 
     * @version (a version number or a date)
     */
    public class Player1 extends Player
    {
        
        private fuerzaBala fuerzabala;    
        /**
         * Act - do whatever the Player1 wants to do. This method is called whenever
         * the 'Act' or 'Run' button gets pressed in the environment.
         */
        public void act() 
        {
                MyWorld w = (MyWorld)getWorld();
                if (isTouching(Bomb.class))
                {
                    w.removeObject(this);
                    Greenfoot.setWorld(new MyWorld());
                }
                
                if(w.turno == true)
                {
                    checkClick();  
                    //w.turno=false;
                }
        }    
        
        
    public void checkClick()
    {
       MyWorld mundo = (MyWorld)getWorld();
       if(Greenfoot.mouseDragged(null)) {
            MouseInfo mouse = Greenfoot.getMouseInfo();
            int dx = mouse.getX()-getX();
            int dy = mouse.getY()-getY();
            if (fuerzabala == null) {
                fuerzabala = new fuerzaBala(dx, dy);
                getWorld().addObject( fuerzabala, getX(), getY() );
            }
            else {
                fuerzabala.setImage(dx, dy);
            }
        }
        if(Greenfoot.mouseDragEnded(null) && fuerzabala != null) {
            getWorld().removeObject(fuerzabala);
            fuerzabala = null;
            MouseInfo mouse = Greenfoot.getMouseInfo();
            Vector force = new Vector(new Location(mouse.getX(), mouse.getY()), new Location(getX(), getY()));
            force.scale(0.2);
            Bomb bala = new Bomb(force);
            mundo.addObject(bala, this.getX()+100, this.getY()-100);
           // mundo.addObject(bala, getX(), getY());
           if(bala.getX()<0)
           {
            mundo.removeObject(bala);
            }
             if(bala.getX()>1000)
            {
                   mundo.removeObject(bala);
            }
           mundo.turno = false;
        }
    } 
}
