import greenfoot.*; // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import javax.swing.JOptionPane;
import java.util.Random;

/**
 * Write a description of class MyWorld here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class MyWorld extends World
{
    ButtonPlay play = new ButtonPlay(); 
    Logo logo = new Logo();
    GreenfootSound music = new GreenfootSound("song.mp3");
    boolean turno = true;
    /**
     * Constructor for objects of class MyWorld.
     * 
     */
    public MyWorld()
    {    
        super(1000, 600, 1);         
                
        addObject(logo,500,-1);  
        addObject(play,500,-1);       
    }
    
    public void Play()
    {
        this.removeObjects(this.getObjects(Logo.class));
        this.removeObjects(this.getObjects(ButtonPlay.class));
        music.playLoop();
        Player1 p1 = new Player1();
        Player2 p2 = new Player2();
        //Start();
        Structure s;    
        int x = 65,y=0;
        for(int i = 0; i < 6; i = i + 1)
        {                  
            s = new Structure();
            // Obtain a number between [450 - 600].        
            y = (int)(Math.random() * ((600 - 450) + 1)) + 450;
            //y = 600;
            addObject(s,x,y);
            if(i==0)
            {
                addObject(p1,75,y-180); 
            }                        
            x = x + 175;                                    
        }   
        addObject(p2,920,y-180); 
        //Bomb b = new Bomb();
        //addObject(b,100,100);
        //Start();
    }
     
    private void Start()
    {        
        String p1Angulo = JOptionPane.showInputDialog("Ángulo Player 1:");
        String p1Velocidad = JOptionPane.showInputDialog("Velocidad Player 1:");
         //ATAQUE P1
        //String p2Angulo = JOptionPane.showInputDialog("Ángulo Player 2:");
        //String p2Velocidad = JOptionPane.showInputDialog("Velocidad Player 2:");
    }
    
    public void removeBomb()
    {
        this.removeObjects(this.getObjects(Bomb.class));
    }
    
    public void removeStruct(Actor s)
    {
         this.removeObject(s);
    }
    
}
