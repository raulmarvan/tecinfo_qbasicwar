import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.*;

/**
 * Write a description of class Player2 here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Player2 extends Player
{
    private fuerzaBala fuerzabala2;
    /**
     * Act - do whatever the Player2 wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
                MyWorld w = (MyWorld)getWorld();
                if (isTouching(Bomb.class))
                {
                    w.removeObject(this);
                    Greenfoot.setWorld(new MyWorld());
                }
                
                if(w.turno==false)
                {
                    checkClick();  
                   // w.turno=true;
                }
    }    
    
    
    public void checkClick()
    {
       MyWorld mundo = (MyWorld)getWorld();
        if(Greenfoot.mouseDragged(null)) {
            MouseInfo mouse = Greenfoot.getMouseInfo();
            int dx = mouse.getX()-getX();
            int dy = mouse.getY()-getY();
            if (fuerzabala2 == null) {
                fuerzabala2 = new fuerzaBala(dx, dy);
                getWorld().addObject( fuerzabala2, getX(), getY() );
            }
            else {
                fuerzabala2.setImage(dx, dy);
            }
        }
        if(Greenfoot.mouseDragEnded(null) && fuerzabala2 != null) {
            getWorld().removeObject(fuerzabala2);
            fuerzabala2 = null;
            MouseInfo mouse = Greenfoot.getMouseInfo();
            Vector force2 = new Vector(new Location(mouse.getX(), mouse.getY()), new Location(getX(), getY()));
            force2.scale(0.2);
            Bomb bala2 = new Bomb(force2);
            mundo.addObject(bala2, this.getX()-100, this.getY()-100);
           // mundo.addObject(bala, getX(), getY());
           if(bala2.getX()<0)
           {
            mundo.removeObject(bala2);
            }
            else if(bala2.getX()>1000)
            {
                   mundo.removeObject(bala2);
            }
           mundo.turno = true;
        }

    } 
}
